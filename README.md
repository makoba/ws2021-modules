# ws2021-modules

These are notes for a talk I am giving in a Workshop on Higher Algebra in the winter term 2021/2022 (organized by Can Yaylali and Torsten Wedhorn).
A compiled version can be found [here](https://makoba.gitlab.io/ws2021-modules/modules.pdf).