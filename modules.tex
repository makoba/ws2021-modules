\documentclass[10pt, a4paper]{scrartcl}

\usepackage{packages}
\usepackage{commands}
\bibliography{references.bib}

\title{Modules}
\author{Manuel Hoff}
\date{}

\begin{document}

    \maketitle

    These are notes for a talk I am giving in a Workshop on \emph{Higher Algebra}, organized by Can Yaylali and Torsten Wedhorn and taking place in the winter term 2021/2022 in Darmstadt.
    References for the material are \cite{HTT} and \cite{HA}.
    There are probably some mistakes in these notes (that are all my fault), so use them at your own risk!

    \section{Notation}

    \subsection{Generalities}

    \begin{itemize}
        \item
        We will say \enquote{category} (resp.\ \enquote{ordinary category}) instead of \enquote{$\infty$-category} (resp.\ \enquote{$1$-category}).
        Being an ordinary category is a property of a category.

        \item
        The categories we will consider will always be presentable.

        \item
        $\catani$ denotes the category of anima.
    \end{itemize}

    \subsection{Ordinary Commutative Algebra}

    \begin{itemize}
        \item
        $\catabs$ denotes the (ordinary) category of Abelian groups.
        It carries a symmetric monoidal structure (given by the tensor product over $\ZZ$) that we denote by $\Tor_0(\blank, \blank)$.

        \item
        $\catcalgs(\catabs)$ denotes the category of commutative algebras in $\catabs$.
        This is just the (ordinary) category of (discrete) commutative rings.

        \item
        Given $R \in \catcalgs(\catabs)$ we denote by $\catmods_R(\catabs)$ the category of modules over $R$ in $\catabs$.
        This is just the (ordinary) category of (discrete) $R$-modules.
        It carries an induced symmetric monoidal structure that we denote by $\Tor^R_0(\blank, \blank)$.
        Using free resolutions in one of the variables we can define the left derived functors
        \[
            \Tor^R_i(\blank, \blank) \colon \catmods_R(\catabs) \times \catmods_R(\catabs) \to \catmods_R(\catabs)
        \]
        for $i \in \ZZ_{\geq 0}$.
    \end{itemize}

    \subsection{Graded Commutative Algebra}

    \begin{itemize}
        \item
        $\catgrabs$ denotes the (ordinary) category of graded Abelian groups.
        It carries a symmetric monoidal structure that we denote by $\Tor_0(\blank, \blank)$ and that is given by
        \[
            \Tor_0(M, N)_i = \bigoplus_{j + k = i} \Tor_0(M_j, N_k)
        \]
        with the symmetry $m \otimes n \mapsto (-1)^{jk} n \otimes m$ for $m \in M_j$ and $n \in N_k$.
        We also have shift functors $[i] \colon \catgrabs \to \catgrabs$ (for $i \in \ZZ$) that are given by $M[i]_j = M_{j - i}$ and $f[i]_j = (-1)^i f_{j - i}$.

        \item
        $\catcalgs(\catgrabs)$ denotes the category of commutative algebras in $\catgrabs$.
        This is just the (ordinary) category of graded commutative graded rings.
        We also write $\catcalgs(\catgrabs)^{\cn} \subseteq \catcalgs(\catgrabs)$ for the full subcategory spanned by those $R$ with $R_i \cong 0$ for $i < 0$.

        \item
        Given $R \in \catcalgs(\catgrabs)$ we denote by $\catmods_R(\catgrabs)$ the category of modules over $R$ in $\catgrabs$.
        This is just the (ordinary) category of graded $R$-modules.
        It carries an induced symmetric monoidal structure that we denote by $\Tor^R_0(\blank, \blank)$.
        $\catmods_R(\catgrabs)$ also carries shift functors $[i]$ as before.

        \item
        Let $R \in \catcalgs(\catgrabs)$.
        Then an object $M \in \catmods_R(\catgrabs)$ is called \emph{graded-free} if it is isomorphic to $\bigoplus_{\alpha \in A} R[i_{\alpha}]$ for some index set $A$ and some $i_{\alpha} \in \ZZ$.
        Using graded-free resolutions in one of the variables we can define the left derived functors
        \[
            \Tor^R_i(\blank, \blank) \colon \catmods_R(\catgrabs) \times \catmods_R(\catgrabs) \to \catmods_R(\catgrabs)
        \]
        for $i \in \ZZ_{\geq 0}$.
        
        We also write $\catmods_R(\catgrabs)^{\geq 0}$ and $\catmods_R(\catgrabs)^{\leq 0}$ for the full subcategories of $\catmods_R(\catgrabs)$ given by those modules that are concentrated in the indicated degrees.
    \end{itemize}

    \subsection{Higher Algebra}

    \begin{itemize}
        \item
        Given a stable category $\calC$ we denote by $[i] \colon \calC \to \calC$ (for $i \in \ZZ$) its shift functors so that $[1]$ is the suspension functor.
        If $\calC$ is equipped with a $t$-structure then we denote by $\calC^{\heartsuit}$ its heart.

        \item
        $\catsp$ denotes the (stable) category of spectra.
        It carries a symmetric monoidal structure (given by the smash product) that we denote by $\blank \otimes \blank$.
        Taking homotopy groups gives rise to a functor $\catsp \to \catgrabs, \; M \mapsto \pi_*(M)$ that is symmetric monoidal and compatible with shifts.
        We write $\catsp^{\cn} \subseteq \catsp$ for the full subcategory spanned by the connective spectra.
        $\catsp^{\cn}$ is part of a $t$-structure of $\catsp$ and $\catsp^{\heartsuit} = \catabs$ via $M \mapsto \pi_0(M)$.
        
        $\catsp^{\cn}$ is stable under the symmetric monoidal structure (and contains the unit object $S$) and the truncation functor $\catsp^{\cn} \to \catabs$ is symmetric monoidal (but the inclusion $\catabs \to \catsp$ is not).

        \item
        $\catcalgs$ denotes the category of commutative algebras in $\catsp$.
        The objects of $\catcalgs$ are called $E_{\infty}$-rings.
        We have an induced functor $\catcalgs \to \catcalgs(\catgrabs), \; R \mapsto \pi_*(R)$.
        We write $\catcalgs^{\cn} \subseteq \catcalgs$ for the full subcategory spanned by the connective $E_{\infty}$-rings.
        Also note that $\catcalgs$ contains $\catcalgs(\catabs)$ as a full subcategory (namely as those $E_{\infty}$-rings $R$ such that $\pi_*(R)$ is concentrated in degree $0$).

        \item
        Given $R \in \catcalgs$ we denote by $\catmods_R$ the category of modules over $R$ in $\catsp$.
        This is a stable category.
        We call the objects of $\catmods_R$ simply $R$-modules.
        We have an induced symmetric monoidal structure on $\catmods_R$ that we denote by $\blank \otimes_R \blank$.

        We have an induced functor $\catmods_R \to \catmods_{\pi_*(R)}(\catgrabs), \; M \mapsto \pi_*(M)$ that is again compatible with shifts (but not symmetric monoidal).
        Write $\catmods_R^{\geq 0}$ (resp.\ $\catmods_R^{\leq 0}$) for the full subcategory of $\catmods_R$ given by those modules $M$ such that $\pi_i(M) \cong 0$ for $i < 0$ (resp.\ $i > 0$).

        \item
        Let $R \in \catcalgs$.
        For $i \in \ZZ$ we write $\Ext_R^i(\blank, \blank) \coloneqq \pi_0 \Map_{\catmods_R}(\blank, \blank [i]) \colon \catmods_R^{\op} \times \catmods_R \to \catabs$.
    \end{itemize}
    In the following, unless explicitly stated otherwise, $R$ always denotes an $E_{\infty}$-ring.

    \section{\texorpdfstring{A $t$-structure on the Category of Modules}{A t-structure on the Category of Modules}}
    
    \begin{proposition}
        Suppose $R \in \catcalgs^{\cn}$.
        Then $\catmods_R^{\geq 0}$ and $\catmods_R^{\leq 0}$ form an accessible $t$-structure on $\catmods_R$.
        The heart $\catmods_R^{\heartsuit}$ identifies with $\catmods_{\pi_0(R)}(\catabs)$ via the functor $M \mapsto \pi_0(M)$.
        Finally $\catmods_R^{\geq 0} \subseteq \catmods_R$ can be characterized as the smallest full subcategory that contains $R$ and is stable under small colimits.
    \end{proposition}

    \begin{proof}
        See \cite[Proposition 7.1.1.13]{HA}.
    \end{proof}

    \section{Free and Projective Modules}

    \begin{definition}
        $M \in \catmods_R$ is called \emph{free} if $M \cong \bigoplus_{\alpha \in A} R$ for some index set $A$.
        It is called \emph{finite free} if the index set $A$ in such a presentation is finite.
    \end{definition}

    \begin{definition}
        Suppose that $R \in \catcalgs^{\cn}$.
        Then a map $M \to N$ in $\catmods_R^{\geq 0}$ is called \emph{surjective} if $\pi_0(M) \to \pi_0(N)$ is surjective.
    \end{definition}

    For the rest of this section we always assume that $R$ is connective.

    \begin{definition}
        Suppose that $R \in \catcalgs^{\cn}$ and let $P \in \catmods_R$.
        Then $P$ is called \emph{projective} if $P \in \catmods_R^{\geq 0}$ and $\Ext_R^i(P, Q) = 0$ for all $Q \in \catmods_R^{\geq 0}$ and $i > 0$.

        $P$ is called \emph{finite projective} if it is projective and $\pi_0(P) \in \catmods_{\pi_0(R)}(\catabs)$ is finitely generated.
    \end{definition}

    \begin{remark}
        Let $\calC$ be a category.
        Then an object $x \in \calC$ is called \emph{projective} if $\Map_{\calC}(x, \blank) \colon \calC \to \catani$ commutes with colimits of shape $\Delta^{\op}$ (these types of colimits are called \enquote{geometric realizations}).

        With this definition the category $\catmods_R$ does not have any nonzero projective objects.
        The projective objects of $\catmods_R^{\geq 0}$ are precisely the projective modules in the sense of the above definition (see \cite[Proposition 7.2.2.6]{HA}).
    \end{remark}

    \begin{proposition} \label{prop:proj-mods}
        Let $P \in \catmods_R^{\geq 0}$.
        The following conditions are equivalent.
        \begin{enumerate}
            \item
            $P$ is projective.

            \item
            $\Ext_R^1(P, Q) = 0$ for all $Q \in \catmods_R^{\geq 0}$ and $i > 0$.

            \item
            $\Ext_R^i(P, Q) = 0$ for all $Q \in \catmods_R^{\heartsuit}$ and $i > 0$.

            \item
            Given a surjective map $N \to N'$ in $\catmods_R^{\geq 0}$ the induced map
            \[
                \Ext_R^0(P, N) \to \Ext_R^0(P, N')
            \]
            is also surjective.

            \item
            Every surjection $M \to P$ in $\catmods_R^{\geq 0}$ splits.

            \item
            $P$ is a direct summand of a free $R$-module.
        \end{enumerate}
        Moreover, $P$ being finite projective is equivalent to $P$ being a direct summand of a finite free module.
    \end{proposition}

    \begin{proof}
        $(i) \Rightarrow (ii), (i) \Rightarrow (iii)$:
        This is obvious.

        $(iii) \Rightarrow (i)$:
        Let $Q \in \catmods_R^{\geq 0}$ and $i > 0$.
        We have fiber sequences
        \[
            \pi_n(Q)[n] \to \tau_{\leq n} Q \to \tau_{\leq n - 1} Q
        \]
        for all $n \in \ZZ_{\geq 0}$ that give rise to exact sequences
        \[
            0 \cong \Ext_R^{i + n}\roundbr[\big]{P, \pi_n(Q)} \to \Ext_R^i\roundbr[\big]{P, \tau_{\leq n}Q} \to \Ext_R^i(P, \tau_{\leq n - 1}Q) \to \Ext_R^{i + n + 1}\roundbr[\big]{P, \pi_n(Q)} \cong 0
        \]
        so that we see that $\Ext_R^i(P, \tau_{\leq n}Q) \cong \Ext_R^i(P, \tau_{\leq n - 1}Q)$.
        Hence we obtain
        \begin{align*}
            \Ext_R^i(P, Q) &\cong \Ext_R^i(P, \varprojlim_n \tau_{\leq n} Q) \\
            &\cong \varprojlim_n \Ext_R^i(P, \tau_{\leq n} Q) \\
            &\cong \Ext_R^i(P, \tau_{\leq -1} Q) \cong \Ext_R^i(P, 0) \cong 0.
        \end{align*}

        $(ii) \Rightarrow (iv)$:
        Let $N \to N'$ be a surjective map in $\catmods_R^{\geq 0}$ and set $N'' \coloneqq \fib(N \to N')$.
        Then the surjectivity of $N \to N'$ implies that we also have $N'' \in \catmods_R^{\geq 0}$.
        Now we have an exact sequence
        \[
            \Ext_R^0(P, N) \to \Ext_R^0(P, N') \to \Ext_R^1(P, N'') \cong 0
        \]
        so that we can conclude.

        $(iv) \Rightarrow (v)$:
        Let $M \to P$ be a surjection in $\catmods_R^{\geq 0}$.
        Then, choosing of a preimage of $\id_P \in \Ext_R^0(P, P)$ under $\Ext_R^0(P, M) \to \Ext_R^0(P, P)$, we obtain a splitting.

        $(v) \Rightarrow (vi)$:
        Set $M \coloneqq \bigoplus_{\pi_0(P)} R \in \catmods_R^{\geq 0}$.
        Then $M$ is free and the natural map $M \to P$ is surjective.
        Applying $(iv)$ we see that $P$ is a direct summand of $M$.

        $(vi) \Rightarrow (i)$:
        Being projective is clearly stable under direct sums and taking direct summands so that it suffices to show that $R$ is projective.
        But this is obvious as $\Ext_R^i(R, Q) \cong \pi_{-i}(Q) \cong 0$ for $Q \in \catmods_R^{\geq 0}$ and $i > 0$.
    \end{proof}
    
    \begin{proposition}
        Let $P \in \catmods_R^{\geq 0}$.
        The following conditions are equivalent:
        \begin{enumerate}
            \item
            $P$ is finite projective.

            \item
            $P$ is a compact projective object in $\catmods_R^{\geq 0}$.

            \item
            $P$ is a direct summand of a finite free module.
        \end{enumerate}
    \end{proposition}

    \begin{proof}
        $(i) \Rightarrow (iii)$:
        Let $A \subseteq \pi_0(P)$ be a finite generating set.
        Then we can replace $M$ in the argument of $(v) \Rightarrow (vi)$ in the proof of \Cref{prop:proj-mods} by $\bigoplus_A R$ so that we see that $P$ is a direct summand of a finite free module.

        $(iii) \Rightarrow (i)$:
        If $P$ is a direct summand of a finite free module $M$ then $\pi_0(P)$ is direct summand of $\pi_0(M) \in \catmods_{\pi_0(R)}(\catabs)$ which is finite free.
        In particular $\pi_0(P)$ is then finitely generated.

        $(ii) \Leftrightarrow (iii)$:
        The forgetful functor $\catmods_R^{\geq 0} \to \catsp^{\geq 0} \xrightarrow{\Omega^{\infty}} \catani$ has a left adjoint $F \colon \catani \to \catmods_R^{\geq 0}$.
        \cite[Corollary 4.7.4.18]{HA} implies in this situation that some $P \in \catmods_R^{\geq 0}$ is compact projective if and only if it is a retract of $F(X)$ for some $X \in \catani$ compact projective.
        Now the compact projective objects in $\catani$ are precisely the finite sets and for such a finite set $X$ we have $F(X) \cong \bigoplus_X R$.
    \end{proof}

    \begin{remark}
        Suppose that $R \in \catcalgs(\catabs)$.
        Then an $R$-module $M \in \catmods_R$ is free/finite free/projective/finite projective if and only if $M \in \catmods_R^{\heartsuit} \cong \catmods_R(\catabs)$ and is free/finite free/projective/finite projective in the usual sense.
    \end{remark}

    \section{\texorpdfstring{The $\Tor$-Spectral Sequence}{The Tor-Specral Sequence}}

    \begin{proposition} \label{prop:tor-ss}
        Given $M, N \in \catmods_R$ there exists a spectral sequence
        \[
            E_2^{p, q} = \Tor^{\pi_*(R)}_p\roundbr[\big]{\pi_*(M), \pi_*(N)}_q \Rightarrow \pi_{p + q}(M \otimes_R N).
        \]
    \end{proposition}

    \begin{proof}
        See \cite[Proposition 7.2.1.19]{HA}.
    \end{proof}

    \begin{corollary} \label{cor:tensor-1}
        Suppose $R \in \catcalgs(\catabs)$ and let $M, N \in \catmods_R$.
        Then we have
        \[
            \Tor^R_p\roundbr[\big]{\pi_*(M), \pi_*(N)}_q \cong \bigoplus_{j + k = q} \Tor^R_p\roundbr[\big]{\pi_j(M), \pi_k(N)}
        \]
        (where the left hand side denotes the $\Tor$-groups over $R$-considered as a graded ring).
        In particular if $M, N \in \catmods_R(\catabs)$ we get
        \[
            \pi_i(M \otimes_R N) \cong \Tor^R_i(M, N).
        \]
    \end{corollary}

    \begin{corollary} \label{cor:tensor-2}
        Suppose $R \in \catcalgs^{\cn}$ and let $M, N \in \catmods_R^{\geq 0}$.
        Then also $M \otimes_R N \in \catmods_R^{\geq 0}$ and
        \[
            \pi_0(M \otimes_R N) \cong \Tor^{\pi_0(R)}_0 \roundbr[\big]{\pi_0(M), \pi_0(N)}.
        \]
    \end{corollary}

    \begin{example}
        Note that in general knowing $\pi_*(R)$, $\pi_*(M)$ and $\pi_*(N)$ is not enough to compute $\pi_*(M \otimes_R N)$ as we they don't determine the differentials that appear in the spectral sequence.
        Here is an example:

        Let $R \coloneqq \ZZ/p^2\ZZ$ for some prime $p$ and consider $M \coloneqq R/pR \oplus R/pR[1]$ and $M'$ given by the complex
        \[
        \begin{tikzcd}
            0
            & R \arrow{l}
            & R \arrow{l}[swap]{p}
            & 0. \arrow{l}
        \end{tikzcd}
        \]
        Then $\pi_*(M) \cong \pi_*(M')$ but one can compute that
        \[
            H_i(R/pR \otimes_R M) \cong
            \begin{cases}
                R/pR &\text{if $i = 0$,} \\
                (R/pR)^2 &\text{if $i > 0$,} \\
                0 &\text{else,}
            \end{cases}
        \]
        while
        \[
            H_i(R/pR \otimes_R M') \cong
            \begin{cases}
                R/pR &\text{if $i = 0, 1$,} \\
                0 &\text{else.}
            \end{cases}
        \]
    \end{example}

    \section{Flat Modules}

    \begin{definition} \label{def:graded-flat}
        Let $R \in \catcalgs(\catgrabs)$ and let $N \in \catmods_R(\catgrabs)$.
        Then $N$ is called \emph{flat} if $N_0 \in \catmods_{R_0}(\catabs)$ is flat (in the usual sense) and the natural map $\Tor^{R_0}_0(R, N_0) \to N$ is an isomorphism (this is the same as saying that the natural map $\Tor^R_0(R_i, N_0) \to N_i$ is an isomorphism for all $i \in \ZZ$).

        Now again suppose that $R \in \catcalgs$.
        Then $N \in \catmods_R$ is called \emph{flat} if $\pi_*(N) \in \catmods_{\pi_*(R)}(\catgrabs)$ is flat in the just defined sense.
    \end{definition}

    \begin{remark}
        Again it is true that if $R \in \catcalgs(\catabs)$ and $N \in \catmods_R$ then $N$ is flat if and only if it is contained in $\catmods_R^{\heartsuit}$ and flat in the usual sense.
    \end{remark}

    \begin{lemma} \label{lem:graded-flat}
        Let $R \in \catcalgs(\catgrabs)$ and let $N \in \catmods_R(\catgrabs)$ be flat.
        Then we have $\Tor^R_i(M, N) \cong 0$ for all $M \in \catmods_R(\catgrabs)$ and $i > 0$.
    \end{lemma}

    \begin{proof}
        We have to show that the functor
        \[
            \Tor^R_0(\blank, N) \colon \catmods_R(\catgrabs) \to \catmods_R(\catgrabs)
        \]
        is (left) exact.
        But by assumption this functor agrees with $\Tor^{R_0}(\blank, N_0)$ and so the exactness follows because $N_0$ is a flat $R_0$ module (in the usual sense).
    \end{proof}

    \begin{corollary} \label{cor:flat}
        Let $M, N \in \catmods_R$ with $N$ flat.
        Then the natural map
        \[
            \Tor^{\pi_0(R)}_0\roundbr[\big]{\pi_i(M), \pi_0(N)} \to \pi_i(M \otimes_R N)
        \]
        is an isomorphism for all $i \in \ZZ$.
        If $R \to S$ is a map in $\catcalgs$ then $S \otimes_R N$ is flat as an $S$-module.
    \end{corollary}

    \begin{proof}
        As $N$ is flat we have
        \[
            \Tor^{\pi_*(R)}_i \roundbr[\big]{\pi_*(M), \pi_*(N)} \cong 0
        \]
        for $i > 0$ and
        \[
            \Tor^{\pi_*(R)}_0 \roundbr[\big]{\pi_*(M), \pi_*(N)} \cong \Tor^{\pi_0(R)}_0 \roundbr[\big]{\pi_*(M), \pi_0(N)}
        \]
        (see \Cref{lem:graded-flat}) so that the result follows from \Cref{prop:tor-ss}.

        The second part immediately follows from the first.
    \end{proof}

    \begin{proposition} \label{prop:flat-mods}
        Suppose $R \in \catcalgs^{\cn}$ and let $N \in \catmods_R^{\geq 0}$.
        Then the following conditions are equivalent.
        \begin{enumerate}
            \item
            $N$ is flat.

            \item
            The functor $\blank \otimes_R N \colon \catmods_R \to \catmods_R$ carries $\catmods_R^{\leq 0}$ into $\catmods_R^{\leq 0}$.

            \item
            The functor $\blank \otimes_R N \colon \catmods_R \to \catmods_R$ carries $\catmods_R^{\heartsuit}$ into $\catmods_R^{\heartsuit}$.

            \item
            $\pi_0(R) \otimes_R N$ is flat as a $\pi_0(R)$-module.
        \end{enumerate}
    \end{proposition}

    To prove this Proposition we need the following Lemma:

    \begin{lemma} \label{lem:flat-mods}
        Let $R \in \catcalgs(\catgrabs)^{\cn}$.
        Let $N \in \catmods_R(\catgrabs)^{\geq 0}$ and suppose that $N_0$ is flat over $R_0$ (in the usual sense) and that the natural map $\Tor^{R_0}_i(R_i, N_0) \to N_i$ is an isomorphism for $i = 1, \dotsc, n - 1$, where $n \geq 1$ is fixed.
        \begin{enumerate}
            \item
            Let $M \in \catmods_R(\catgrabs)^{\geq 0}$.
            Then we have
            \[
                \Tor^R_i(M, N)_j \cong 0
            \]
            for all $i > 0$ and $j < n$ and
            \[
                \Tor^R_0(M, N)_j \cong \Tor^{R_0}_0(M_j, N_0)
            \]
            for $j < n$.

            \item
            We have
            \[
                \Tor^R_0(R_0, N)_n \cong \coker\roundbr[\big]{\Tor^{R_0}_0(R_n, N_0) \to N_n}.
            \]

            \item
            Suppose that $\Tor^{R_0}_n(R_n, N_0) \to N_n$ is surjective.
            Then we have
            \[
                \Tor^R_1(R_0, N)_n \cong \ker\roundbr[\big]{\Tor^{R_0}_0(R_n, N_0) \to N_n}.
            \]
        \end{enumerate}
    \end{lemma}

    \begin{proof}
        $(i)$:
        There should be a slick way to do this, but I don't see how it goes.

        $(ii)$:
        Applying the definition of $\Tor^R_0(\blank, \blank)$ we see that $\Tor^R_0(R_0, N)_n$ is given as the coequalizer of two maps
        \[
            \bigoplus_{i = 0}^n \Tor^{R_0}_0(R_i, N_{n - i}) \to N_n
        \]
        one of which is the projection $\bigoplus_i \Tor^{R_0}_0(R_i, N_{n - i}) \to \Tor^{R_0}_0(R_0, N_n) \cong N_n$ and one of which is the multiplication map.
        Applying our assumption we see that this is the same as the cokernel of $\Tor^{R_0}_0(R_n, N_0) \to N_n$ as claimed.

        $(iii)$:
        Define $K \coloneqq \ker(R \to R_0) \in \catmods_R(\catgrabs)$ so that $K_i = R_i$ for $i < 0$ and $K_i = 0$ otherwise.
        We then have a short exact sequence $0 \to K \to R \to R_0 \to 0$ that gives rise to an exact sequence
        \[
            0 \cong \Tor^R_1(R, N) \to \Tor^R_1(R_0, N) \to \Tor^R_0(K, N) \to \Tor^R_0(R, N) \cong N.
        \]
        Now $\Tor^R_0(K, N)_n$ is given by the coequalizer of the two obvious maps
        \[
            \bigoplus_{\substack{a + b + c = n \\ a > 0}} \Tor^{R_0}_0(R_a, R_b, N_c) \to \bigoplus_{\substack{a + c =n \\ a > 0}} \Tor^{R_0}_0(R_a, N_c).
        \]
        Using that the maps $\Tor^{R_0}_0(R_i, N_0) \to N_i$ are surjective for $i = 1, \dotsc, n$ we see that this coequalizer is given by $\Tor^{R_0}(R_n, N_0)$.
        The above exact sequence thus implies that $\Tor^R_1(R_0, N) \cong \ker \roundbr[\big]{\Tor^{R_0}_0(R_n, N_0) \to N_n}$ as desired.
    \end{proof}

    \begin{proof}[Proof of \Cref{prop:flat-mods}]
        $(i) \Rightarrow (ii)$:
        This follows directly from \Cref{cor:flat}.

        $(ii) \Rightarrow (iii)$:
        This follows directly from \Cref{cor:tensor-2}.

        $(iii) \Rightarrow (iv)$:
        The restriction of $\blank \otimes_R N$ to $\catmods_R^{\heartsuit}$ (that carries it into itself) is given by $\Tor^{R_0}_0\roundbr[\big]{\blank, \pi_0(N)}$ so that we see that $\pi_0(N) \cong \pi_0\roundbr[\big]{\pi_0(R) \otimes_R N}$ is flat over $\pi_0(R)$ (in the usual sense).
        Moreover $\pi_0(R) \otimes_R N$ is also contained in $\catmods_R^{\heartsuit}$ so that we see that it is flat over $\pi_0(R)$.

        $(iv) \Rightarrow (i)$:
        $\pi_0(N) \cong \pi_0\roundbr[\big]{\pi_0(R) \otimes_R N}$ is flat over $\pi_0(R)$ (in the usual sense) by assumption.
        We now inductively show that the multiplication maps $\Tor^{\pi_0(R)}_0\roundbr[\big]{\pi_n(R), \pi_0(N)} \to \pi_n(N)$ are isomorphisms for all $n \in \ZZ_{\geq 0}$.
        For $n = 0$ this is clear so assume that $n > 0$.

        We then know from \Cref{lem:flat-mods} that $\Tor^{\pi_*(R)}_i (\pi_0(R), \pi_*(N))_j \cong 0$ for $j < n$ an $(i, j) \neq (0, 0)$.
        As we know that $\pi_0(R) \otimes_R N \in \catmods_{R_0}^{\heartsuit}$ we can deduce from \Cref{prop:tor-ss} that $\Tor^{\pi_*(R)}_i (\pi_0(R), \pi_*(N))_n \cong 0$ for $i = 0, 1$ so that we can conclude the induction step by \Cref{lem:flat-mods}.
    \end{proof}

    \begin{theorem}[Lazard's Theorem]
        Suppose $R \in \catcalgs^{\cn}$ and let $N \in \catmods_R^{\geq 0}$.
        Then the following conditions are equivalent.
        \begin{enumerate}
            \item
            $N$ is a filtered colimit of finite free modules.

            \item
            $N$ is a filtered colimit of projective modules.

            \item
            $N$ is flat.
        \end{enumerate}
    \end{theorem}

    \begin{proof}
        See \cite[Theorem 7.2.2.15]{HA}.
    \end{proof}

    \section{Perfect Modules}

    \begin{definition}
        We define $\catmods_R^{\perf} \subseteq \catmods_R$ to be the smallest stable subcategory that contains $R$ and is closed under retracts.

        A module $M \in \catmods_R$ is called perfect if it is contained in $\catmods_R^{\perf}$.
    \end{definition}

    \begin{proposition}
        Let $M \in \catmods_R$.
        The following conditions are equivalent.
        \begin{enumerate}
            \item
            $M$ is perfect.

            \item
            $M$ is a compact object of $\catmods_R$.

            \item
            There exists $M^{\vee} \in \catmods_R$ such that $\Map_{\catmods_R}(M, \blank) \colon \catmods_R \to \catani$ is isomorphic to the composition
            \[
                \catmods_R \xrightarrow{M^{\vee} \otimes_R \blank} \catmods_R \to \catsp \xrightarrow{\Omega^{\infty}} \catani.
            \]
        \end{enumerate}
    \end{proposition}

    \begin{proof}
        See \cite[Proposition 7.2.4.4]{HA}.
    \end{proof}

    \printbibliography
\end{document}